//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//EJ1- Escribir un archivo con extensión .js donde se declare un vector de 6 elementos, usando 3 tipos de datos JS
let vector = [11,'Texto1',true,22,false,'Texto2'];
//a. Imprimir en la consola el vector
for (let i=0; i < vector.length;i++) {
  console.log(vector[i]);
};
//b. Imprimir en la consola el primer y el último elemento del vector usando sus índices
console.log(vector[0]);
console.log(vector[vector.length - 1]);
//c. Modificar el valor del tercer elemento
vector[2] = false;
//d. Imprimir en la consola la longitud del vector
console.log(vector.length);
//e. Agregar un elemento al vector usando "push"
vector.push('NuevoElemento');
//f. Eliminar elemento del final e imprimirlo usando "pop"
let ultimoElemento = vector.pop();
console.log(ultimoElemento);
//g. Agregar un elemento en la mitad del vector usando "splice"
let mitad = Math.ceil(vector.length / 2);
vector.splice(mitad, 0, 'NuevoElemento');
//h. Eliminar el primer elemento usando "shift"
let primerElemento = vector.shift();
//i. Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vector.unshift(primerElemento);
console.log(vector);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
