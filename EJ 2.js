//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//EJ2- Escribir un archivo con extensión .js donde se declare un vector de 6 elementos, usando 3 tipos de datos JS:
let vector = [11,'Texto1',true,22,false,'Texto2'];
//a. Imprimir en la consola cada valor usando "for"
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
};
//b. Idem al anterior usando "forEach"
vector.forEach(function(valor) {
  console.log(valor);
});
//c. Idem al anterior usando "map"
vector.map(function(valor) {
  console.log(valor);
});
//d. Idem al anterior usando "while"
let i = 0;
while (i < vector.length) {
  console.log(vector[i]);
  i++;
}
//e. Idem al anterior usando "for..of"
for (let valor of vector) {
  console.log(valor);
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
